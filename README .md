# playwright_tests

Playwright tests repo is an example of playwright tests implemented using PageObject pattern and fixtures for learning purposes

## Requirements
NodeJS 14 or higher

## Installation

```bash
npm install
```

## Usage

### Run tests:
```bash
npm run test
```

### Launch tests in UI mode:
```bash
npm run test-ui
```

### Test report
/playwright-report/index.html

### Run eslint rules and fix errors if possible:
```bash
npm run lint
```

## License

ISC
